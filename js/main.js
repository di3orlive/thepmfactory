jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};


(function() {
    $(document).ready(function() {

        if ($.exists(".tableSlider")) {
            $('.tableSlider').bxSlider({
                nextSelector: '#slider-next',
                prevSelector: '#slider-prev',
                nextText: '',
                prevText: '',
                pager: false
            });
        }

        if ($.exists("#workSlider1")) {
            var slider1 = $('#workSlider1').lightSlider({
                item: 1,
                enableDrag: true,
                useCSS: false,
                pager: false,
                adaptiveHeight: true,
                speed: 200
            });
            $('#workSlider1-prev').click(function(){
                slider1.goToPrevSlide();
            });
            $('#workSlider1-next').click(function(){
                slider1.goToNextSlide();
            });
        }

        if ($.exists("#workSlider2")) {
            var slider2 = $('#workSlider2').lightSlider({
                item: 1,
                enableDrag: true,
                useCSS: false,
                pager: false,
                adaptiveHeight: true,
                speed: 200
            });
            $('#workSlider2-prev').click(function(){
                slider2.goToPrevSlide();
            });
            $('#workSlider2-next').click(function(){
                slider2.goToNextSlide();
            });
        }

        if ($.exists("#workSlider3")) {
            var slider3 = $('#workSlider3').lightSlider({
                item: 1,
                enableDrag: true,
                useCSS: false,
                pager: false,
                adaptiveHeight: true,
                speed: 200
            });
            $('#workSlider3-prev').click(function(){
                slider3.goToPrevSlide();
            });
            $('#workSlider3-next').click(function(){
                slider3.goToNextSlide();
            });
        }

        if ($.exists("#workSlider4")) {
            var slider4 = $('#workSlider4').lightSlider({
                item: 1,
                enableDrag: true,
                useCSS: false,
                pager: false,
                adaptiveHeight: true,
                speed: 200
            });
            $('#workSlider4-prev').click(function(){
                slider4.goToPrevSlide();
            });
            $('#workSlider4-next').click(function(){
                slider4.goToNextSlide();
            });
        }

        if ($.exists("#workSlider5")) {
            var slider5 = $('#workSlider5').lightSlider({
                item: 1,
                enableDrag: true,
                useCSS: false,
                pager: false,
                adaptiveHeight: true,
                speed: 200
            });
            $('#workSlider5-prev').click(function(){
                slider5.goToPrevSlide();
            });
            $('#workSlider5-next').click(function(){
                slider5.goToNextSlide();
            });
        }


//======================================================================================================================


        if($.exists(".scroll-nav")) {
            $('.nav a').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').stop().animate({
                            scrollTop: target.offset().top - 70
                        }, 1000);
                        return false;
                    }
                }
            });


            $(window).scroll(function () {
                var top = $(document).scrollTop();
                if (top > 10) {
                    $('.header').addClass('reduce-header');
                }
                else {
                    $('.header').removeClass('reduce-header');
                }
            });
        }


        if ($.exists(".parallax")) {
            function parallaxCheck(){
                //Illustration by http://psdblast.com/flat-color-abstract-city-background-psd
                $(window).on('mousemove', function(e) {
                    var w = $(window).width();
                    var h = $(window).height();
                    var offsetX = 0.5 - e.pageX / w;
                    var offsetY = 0.5 - e.pageY / h;

                    $(".parallax").each(function(i, el) {
                        var offset = parseInt($(el).data('offset'));
                        var translate = "translate3d(" + Math.round(offsetX * offset) + "px," + Math.round(offsetY * offset) + "px, 0px)";

                        $(el).css({
                            '-webkit-transform': translate,
                            'transform': translate,
                            'moz-transform': translate
                        });
                    });
                });
            }

            $( window ).resize(function() {
                if($(window).width() >= 1201){
                    parallaxCheck();
                }
            });

            if($(window).width() >= 1201){
                if($.exists(".parallax")) {
                    parallaxCheck();
                }
            }
        }


        if ($.exists(".sticky")) {
            var stickyTop = $('.sticky').offset().top; // returns number

            $(window).scroll(function(){ // scroll event

                var windowTop = $(window).scrollTop(); // returns number

                if (stickyTop - 100 < windowTop) {
                    $('.sticky').addClass('stickyBox');
                }
                else {
                    $('.sticky').removeClass('stickyBox');
                }

            });
        }


        if ($.exists(".loadMoreBtn")) {
            $('.loadMoreBtn').on('click', function(e){
                e.preventDefault();
                $('.hiddenTestimonials').slideDown();
            });
        }


        if ($.exists(".navBtn")) {
            $('.navBtn').on('click', function(){
                $('.nav').stop().slideToggle('fast');
                $('.navBtn').toggleClass('active');
            });
        }


        if ($.exists("")) {

        }


    });
}).call(this);


